port module EraLegisPicker exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Url.Builder as Ub
import Time
import Task
import Browser
import String exposing (isEmpty)
import Json.Decode as Jdecode exposing (string, int, field, maybe, decodeValue, float, at)
import Json.Encode as Jencode exposing (Value)

port georeq : Value -> Cmd msg
port geores : (Value -> msg) -> Sub msg

main =
    Browser.element
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

-- MODEL
type alias Model =
    { isoDate : String
    , templateStr : Maybe String
    , timeZone : String
    , location : Maybe Coord
    , allTimeZones : List (String, String)
    , solarDate: String
    , language: String
    , solarDateElement: String
    , apiBaseUri: String
    , dateError: Maybe String
    , showExtra: Bool
    }

type alias Coord = {lat : Float, lon : Float}

languageOptions =
    [ ("symbol", "Symbols")
    , ("latin", "Latin")
    , ("english", "English")
    , ("poor-latin", "Bad Latin")
    ]

init flags =
    let
        strFlagWithDefault flag default =
            case (decodeValue (field flag string) flags) of
                Ok s -> s
                Err s -> default
        date = strFlagWithDefault "date" ""
        solarDateElement = strFlagWithDefault "solarDateElement" "div"
        apiBaseUri = strFlagWithDefault "apiBaseUri" "//date.eralegis.info"
        timeZoneOptions =
            [ ("US/Eastern", "US/Eastern")
            , ("US/Central", "US/Central")
            , ("US/Mountain", "US/Mountain")
            , ("US/Pacific", "US/Pacific")
            , ("UTC", "UTC")
            ]
        defaultLanguage =
            List.head languageOptions
                |> Maybe.withDefault ("symbol", "Symbols")
                |> Tuple.first
        startingModel =
            { isoDate = date
            , templateStr = Nothing
            , timeZone = "UTC"
            , location = Nothing
            , allTimeZones = timeZoneOptions
            , solarDate = ""
            , language = defaultLanguage
            , solarDateElement = solarDateElement
            , apiBaseUri = apiBaseUri
            , dateError = Nothing
            , showExtra = False
            }
    in
        ( startingModel, Task.perform GetLocalZone Time.getZoneName )

-- UPDATE
type Msg
    = SubmitRequest
      | GetLocalZone Time.ZoneName
      | RequestGeo
      | ReceiveGeo (Maybe Coord)
      | SetLatLon (Coord -> Float -> Coord) String
      | IsoDateChange String
      | TemplateChange String
      | SetTimeZone String
      | SetLanguage String
      | SetShowExtra Bool
      | ReceivedNewThelemicDate (Result Http.Error String)

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        SubmitRequest ->
            (model, getThisDate model)

        GetLocalZone zn ->
            let
                tzOffsetToIso offset =
                    let
                        sign = if offset <= 0 then "+" else "-"
                        isoOffset = (abs offset) * 10 // 6
                        padLength = 4 - (String.fromInt isoOffset |> String.length)
                        pad = String.repeat padLength "0"
                        strInt = String.fromInt isoOffset
                    in
                        sign ++ pad ++ strInt
                znstr =
                    case zn of
                        Time.Name x -> x
                        Time.Offset o -> tzOffsetToIso o
                znstrlabel = "Auto (" ++ znstr ++ ")"
                newAtz = (znstr, znstrlabel) :: model.allTimeZones
                newmod = {model | timeZone = znstr, allTimeZones = newAtz}
            in
                (newmod, getThisDate newmod)

        RequestGeo ->
            (model, Jencode.null |> georeq)

        ReceiveGeo x ->
            let
                newmod = {model | location = x}
            in
                (newmod, getThisDate newmod)

        SetLatLon assign s ->
            let
                oldCoord =
                    case model.location of
                        Nothing -> {lat = 0.0, lon = 0.0}
                        Just c -> c
                f = String.toFloat s |> Maybe.withDefault 0.0
                newCoord = assign oldCoord f
                loc = if isEmpty s then Nothing else Just newCoord
            in
                ({model | location = loc}, Cmd.none)

        IsoDateChange isoDate ->
            ( {model | isoDate = isoDate}, Cmd.none)

        TemplateChange t ->
            ( {model | templateStr = if isEmpty t then Nothing else Just t}, Cmd.none)

        SetTimeZone zone ->
            let
                newmod = {model | timeZone = zone}
            in
                ( newmod, getThisDate newmod )

        SetLanguage lang ->
            let
                newmod = {model | language = lang}
            in
                (newmod, getThisDate newmod)

        SetShowExtra show ->
            ({model | showExtra = show}, Cmd.none)

        ReceivedNewThelemicDate (Ok solarDate) ->
            ( {model | solarDate = solarDate, dateError = Nothing}, Cmd.none )

        ReceivedNewThelemicDate (Err e) ->
            let
                errmsg =
                    case e of
                        Http.BadUrl s -> "Bad URL: " ++ s
                        Http.Timeout -> "Network access timed out"
                        Http.NetworkError -> "Network error"
                        Http.BadStatus i ->
                            case i of
                                400 -> "Unsupport date or date format"
                                _ -> "Unknown error"
                        Http.BadBody s -> s
            in
                ({model | dateError = Just errmsg}, Cmd.none)

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions model =
    let
        decodeGeo x =
            let
                coord = Jdecode.map2 Coord
                        (at ["coords", "latitude"] float)
                        (at ["coords", "longitude"] float)
                res = case (decodeValue coord x) of
                          Ok s -> Just s
                          Err _ -> Nothing
            in
                ReceiveGeo res
    in
        geores (decodeGeo)

-- VIEW
view : Model -> Html Msg
view model =
    div []
        [ dateDisplay model, formDisplay model ]

dateDisplay : Model -> Html Msg
dateDisplay model =
    case model.dateError of
        Just e -> node model.solarDateElement [class "solarDateError"] [text e]
        Nothing ->  node model.solarDateElement [class "solarDate"] [text model.solarDate]

formDisplay : Model -> Html Msg
formDisplay model =
    let
        splitOption current o = option
                    [ selected <| current == Tuple.first o, value <| Tuple.first o ]
                    [ text <| Tuple.second o ]
        onChange f = Jdecode.map f targetValue |> on "change"
        onText f = Jdecode.succeed f |> on "change"
        dateInput = input
                    [ type_ "text"
                    , title "Input date (YYYY-MM-DD HH:MM:SS)"
                    , placeholder "1904-03-20 12:00:00"
                    , onInput IsoDateChange
                    , onText SubmitRequest
                    ]
                    []
        languageSelect = select
                    [ onChange SetLanguage
                    , class "pickerLanguageSelect"
                    , title "Select expression style"
                    ]
                    <| List.map (splitOption model.language) languageOptions
        tzSelect = select
                    [ onChange SetTimeZone
                    , class "pickerTzSelect"
                    , title "Select time zone"
                    ]
                    <| List.map (splitOption model.timeZone) model.allTimeZones
        templateInput = input
                   [ type_ "text"
                   , title "Output template"
                   , placeholder "Sol in {ssign}, Luna in {lsign}..."
                   , model.templateStr |> Maybe.withDefault "" |> value
                   , onInput TemplateChange
                   , onText SubmitRequest
                   ]
                   []
        geoButton = button
              [ onClick RequestGeo ]
              [ text "Get my location" ]

        geoInputs =
            let
                mayFloat acc =
                    case model.location of
                        Just c -> acc c |> String.fromFloat
                        Nothing -> ""
            in
                div []
                     [ label [] [ text "Lat: "]
                     , input
                          [ type_ "text"
                          , title "Latitude"
                          , placeholder "45.1023"
                          , mayFloat (.lat) |> value
                          , SetLatLon (\c f -> {c | lat = f}) |> onChange
                          ] []
                    , label [] [text "Lon: "]
                    , input
                          [ type_ "text"
                          , title "Latitude"
                          , placeholder "-122.4180"
                          , mayFloat (.lon) |> value
                          , SetLatLon (\c f -> {c | lon = f}) |> onChange
                          ] []
                    ]

        showExtraCheckbox =
            label []
                [ input
                      [ type_ "checkbox"
                      , title "Toggle extra options"
                      , onCheck SetShowExtra
                      ]
                      []
                , text "Options"
                ]
        submitButton = button
              [ onClick SubmitRequest ]
              [ text "submit" ]
        extraOptions =
            case model.showExtra of
                True ->
                    [ br [] []
                    , templateInput
                    , tzSelect
                    , br [] []
                    , geoInputs
                    , geoButton
                    ]
                False -> []
    in
        div [ class "pickerControls" ] <|
            [ dateInput, languageSelect, showExtraCheckbox ]
            ++ extraOptions
            ++ [ br [] [], submitButton ]

-- HTTP
getThisDate : Model -> Cmd Msg
getThisDate model =
    let
        alwaysParams =
              [ Ub.string "format" "txt"
              , Ub.string "lang" model.language
              , Ub.string "tz" model.timeZone
              ]
        templateParam =
            case model.templateStr of
                Just s -> [(Ub.string "template" s)]
                Nothing -> []
        geoParam =
            case model.location of
                Just c ->
                    let
                        str = (String.fromFloat c.lat) ++ ":" ++ (String.fromFloat c.lon)
                    in
                        [(Ub.string "location" str)]
                Nothing -> []
        params = alwaysParams ++ templateParam ++ geoParam
        url = Ub.crossOrigin model.apiBaseUri
              [ model.isoDate ]
              params
    in
        Http.get {url = url, expect = Http.expectString ReceivedNewThelemicDate }
